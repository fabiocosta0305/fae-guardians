---
tags: Personagens, PCs, Icônicos, Pinóquio, Circo di Stromboli, Guardiões
---
 
# Robert "Lighthands Bobby" McCarthy, o Trapaceiro sem Cordões

## Aspectos
 
|        ***Tipo*** | ***Aspecto***                                                             |
|------------------:|---------------------------------------------------------------------------|
|    ***Conceito*** | Trapaceiro Honesto Portador do _Pinóquio_                                 |
| ***Dificuldade*** | Freios morais flexíveis que o metem em ___muita___ encrenca               |
|     ***Abdução*** | Um Burrinho de Madeira no Teatro de Marionetes                            |
|     ***Retorno*** | _Grilo Falante_, o amigo nas horas difíceis e a consciência inconveniente |
|   ***Adaptação*** | Aprender a Confiar e a ser confiável                                      |

## Perícias
 
|         ***Nível*** | ***Perícia*** | ***Perícia*** | ***Perícia*** | ***Perícia*** |
|--------------------:|:-------------:|:-------------:|:-------------:|:-------------:|
|    ***Ótimo (+4)*** | Roubo         |               |               |               |
|      ***Bom (+3)*** | Furtividade   | Atletismo     |               |               |
| ***Razoável (+2)*** | Provocar      | Empatia       | Comunicação   |               |
|  ***Regular (+1)*** | Ofícios       | Conhecimentos | Lutar         | Vontade       |

## Façanhas [Recarga: 3]
  
+ ___Sempre há uma saída___
+ ___Gírias das Ruas___
+ __*Não há cordões em mim!* (Permissão: *Escopo do Pinóquio*):__ +2 ao _Defender-se_ de qualquer tentativa de o aprisionar, amarrar ou de outro modo impedí-lo de se deslocar. Além disso, pode gastar _Estresse_ e _Condições de Escopo_ como _Esforço Extra_ em Ações que envolvam Superar obstáculos físicos, ou de Criar Vantagens envolvendo pequenas trapaças por Roubo.

## Estresse

+ ___Estresse:___
    + ___Físico:___ (1)(2)( )( )
    + ___Mental:___ (1)(2)(3)( )
+ ___Consequências:___
    + ___Suave (2):___ 
    + ___Moderada (4):___ 
    + ___Severa (6):___ 
+ ___Condições:___
    + ___Escopo:___ ( )( )( )( )( )


