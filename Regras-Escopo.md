---
tags: Regras, Guardiões, Escopo
---

# Guardiões
 
O multiverso, onde a nossa realidade é apenas uma entre muitas, demanda Guardiões, pessoas que protegem as múltiplas realidades das consequências das transições e interações entre elas. Guardiões surgem quando certas Entidades mais poderosas de uma ou outra realidade abduz pessoas e as imbuí com parte de seu poder e essência. Essas pessoas passam a serem capazes de passar de uma realidade para a outra, além de poderem beber desse poder e essência imbuídos neles, mas muitas vezes pagam o preço de se arriscarem a se perder nessa situação.
 
## Condições
 
 + _Escopo (Estável):_  ( )( )( )( )( ) essas Condições representam o quão próximo você está de perder-se, ao menos por um tempo, em meio à essência que foi imbuída em você, comportando-se como tal criatura. Isso pode ou não ser bom, mas seja como for, durante esse período você não é você. Quanto mais _Escopo_ você marcar, mais seu comportamento e aparência serão influenciados pela essência imbuída em você: com 1, talvez seus olhos fiquem amarelentos e com pupilas retas, ou seu cabelo esbranqueça levemente e suas feições se tornem suaves e bonachonas. Com 5, você basicamente vira um lobo gigante com sede de sangue (preferencialmente de crianças), ou você vire uma velha bondosa e sorridente, cantante e quase nojenta de tão doce! Você pode marcar Escopo como _Esforço Extra_ em suas ações. Limpe uma caixa por sessão ou sempre que puder fazer uma ação que vá de acordo com seu _Escopo_. Limpe todas se você entrar em _Sobrecarga_, perdendo o controle do seu personagem até o final da essa cena ou da próxima. Ao preencher a quinta caixa de Escopo, você automaticamente entra em _Sobrecarga_.
 
## Façanhas Básicas
 
+ _Reconhecer:_ qualquer Guardião pode reconhecer a presença de criaturas ou energias sobrenaturais, ainda que não consiga determinar exatamente do que se trata. Role _Percepção_, com dificuldade dependendo do quanto essa energia está liberada: perceber um  Guardião em Sobrecarga pode ser _Regular (+1)_, enquanto uma criatura se ocultando e suprimindo sua aura de magia pode ser _Excepcional (+5)_. Uma _Falha_ pode demandar marcar uma caixa de _Escopo_ para ser bem-sucedido, ou dar sua localização a um alvo. Você normalmente será capaz de perceber distância, direção ou intenções do alvo. Apenas com um _Sucesso com estilo_ você consegue tudo isso ao mesmo tempo;
 
## Façanhas Opcionais
 
+ _Espantar:_ Pessoas comuns, sem poderes excepcionais, podem se aparovar com a presença de um Guardião. Isso pode ir desde uma leve ojeriza, como se o Guardião não tivesse tomado banho no dia ou estive com algum odor corporal estranho, até mesmo trazer a ela uma visão real do poder mágico, apavorando-a diante de algo realmente fora da realidade. Marque um de Escopo e receba +3 no próximo teste de _Provocar_.
+ _Encantar:_ Você pode imbuir um objeto magicamente com parte da energia ou essência imbuída em você. Qualquer pessoa que porte ou consuma esse objeto pode ser rastreado por você enquanto ele estiver de posse de tal objeto. Além disso, enquanto a pessoa tiver esse objeto ou o consumir, ela estará sobre efeito de um encanto simples e efetivo, aumentando sua chance de sujeitar a vontade dessa pessoa à sua. Marque 1 caixa de _Escopo_. Isso trará para jogo um Aspecto sem Invocações Gratuita de ___Objeto Encantado___. Enquanto a pessoa possuir esse objeto ou consumir o mesmo (tipo, uma barra de chocolate, enquanto o organismo estiver o metabolizando), não será necessário fazer rolamentos para a _Reconhecer_ (apenas você; outros Guardiões e similares recebem +2 nos rolamentos para a Reconhecer). Além disso, o personagem pode eliminar esse Aspecto para obter os mesmos efeitos de um Impulso (o que na prática ele é).
+ _Fascinar (Requer Encantar):_ pessoas sob efeito de _Encantar_ são facilmente Fascináveis. Você recebe +2 em todos os testes de _Comunicação_ enquanto essa pessoa estiver sob efeito de _Encantar_
+ _( ) Patrono  (Permissão: Aspecto indicando o Patrono):_ Seu Guardião possui um poderoso patrono, alguém que pode lhe ajudar em momentos de necessidade. Você pode marcar essa caixa para conseguir alguns dos benefícios abaixo, e para limpar a caixa você deve cumprir algum tipo de favor ao seu Patrono (normalmente de valor similar ao usado, mas pode ser qualquer tipo):
     + Seu Patrono irá lhe oferecer alguns capangas para lutar ou prestar pequenos serviços em seu nome. Eles não são exatamente espertos ou poderosos (_Qualidade **Regular (+1)**_), mas podem ser úteis em suas limitações;
     + Seu Patrono _per se_ pode cumprir algum favor para você. Entretanto, ele nunca fará nada que vai contra seus objetivos.
     + Seu Patrono pode ser questionado sobre algo e responderá de maneira honesta e verdadeira. Ele ainda assim poderá ocultar e omitir informações, e ele não será onisciente.

