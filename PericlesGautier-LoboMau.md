---
tags: Personagens, PCs, Icônicos, Lobo Mau, Floresta Sombria, Guardiões
---

# Pericles Gautier, o Investigador com a Besta Interior

## Aspectos
 
|        ***Tipo*** | ***Aspecto***                                                        |
|------------------:|----------------------------------------------------------------------|
|    ***Conceito*** | Investigador Particular Portador do _Lobo Mau_                       |
| ***Dificuldade*** | Vem Fácil, Vai Fácil                                                 |
|     ***Abdução*** | O Cheiro de Sangue da Floresta de Pedra e da Floresta Negra          |
|     ***Retorno*** | Os Instintos de um Predador são difíceis de serem contidos           |
|   ***Adaptação*** | Helen é boazinha demais, alguém tem que ter o pé no chão nessa coisa |

## Perícias

| ***Nível*** | ***Perícia*** | ***Perícia*** |  ***Perícia*** |  ***Perícia*** | 
|-:|:-:|:-:|:-:|:-:|
| ***Ótimo (+4)*** | Investigar | | | | 
| ***Bom (+3)*** | Percepção | Contatos | | | 
| ***Razoável (+2)*** | Lutar | Atirar | Atletismo | | 
| ***Regular (+1)*** | Empatia | Provocar | Comunicação | Vigor |
 
## Façanhas [Recarga: 3]
  
+ ___Poder da Dedução___
+ ___O Velho e bom 1-2:___ ao Atacar com _Lutar_ e sendo bem-sucedido com estilo, pode optar por, baixando 1 no Estresse, provocar um novo Ataque. Esse novo ataque __Não__ obtém o benefício dessa Façanha. 
+ __*A Besta Interior* (Permissão: *Escopo do Lobo Mau*):__ _Uma vez por cena_ pode invocar sua Besta Interior e trazer um benefício específico que entra em jogo como um ___Aspecto___. Enquanto o ___Aspecto___ estiver em jogo, Pericles continua recebendo os benefícios do mesmo. Pericles pode trazer outros benefícios adicionais, ao custo de 1 Condição de Escopo por benefício. Caso isso resulte em uma ___Sobrecarga de Escopo___, Pericles se transforma em uma ___Fera Descontrolada___. Todos os benefícios se cessam ao final da cena ou assim que Pericles desejar.
   + ___"Que Olhos Grandes!"___ - +2 em todos os testes de _Investigar_ e _Percepção_ envolvendo a visão, e não pode ter Aspectos de Cena relativos a baixa ou alta luminosidade Forçados contra ele, exceto pelo próprio __*"Que Olhos Grandes!"*__.
   + ___"Que Orelhas Grandes!"___ - +2 em todos os testes de _Investigar_ e _Percepção_ envolvendo a audição, e não pode ter Aspectos de Cena relativos a ruído alto ou a silêncio Forçados contra ele, exceto pelo próprio __*"Que Orelhas Grandes!"*__.
   + ___"Que Nariz Grande!"___ - +2 em todos os testes de _Investigar_ e _Percepção_ envolvendo o olfato, e não pode ter Aspectos de Cena relativos a excessos ou ausência de cheiros Forçados contra ele, exceto pelo próprio __*"Que Nariz Grande!"*__.
   + ___"Que Boca Grande!"___ +2 em _Lutar_ usando mordidas. É considerada uma _Arma: 2_ para efeito de dano
   + ___"Que Mãos Grandes!"___ +2 em _Lutar_ usando garras. É considerada uma _Arma: 2_ para efeito de dano
   + ___"Que Pernas Grandes!"___ +2 em todos os testes de _Atletismo_, e não pode ter Aspectos de Cena relacionados a Barreiras Físicas Forçados contra ele, exceto pelo próprio __*"Que Pernas Grandes!"*__

## Estresse

+ ___Estresse:___
    + ___Físico:___ (1)(2)(3)( )
    + ___Mental:___ (1)(2)( )( )
+ ___Consequências:___
    + ___Suave (2):___ 
    + ___Moderada (4):___ 
    + ___Severa (6):___ 
+ ___Condições:___
    + ___Escopo:___ ( )( )( )( )
