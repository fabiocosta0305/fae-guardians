---
tags: Personagens, PCs, Icônicos, Fadas Madrinhas, Salão das Fadas, Guardiões
---


# Helen Hemingway, a Madrinha Filantropa

## Aspectos
 
|        ***Tipo*** | ***Aspecto***                                                     |
|------------------:|-------------------------------------------------------------------|
|    ***Conceito*** | Socialite Filantropa Portadora da _Fada Madrinha_                 |
| ***Dificuldade*** | Pobre Menina Rica                                                 |
|     ***Abdução*** | De um Castelo de Fadas para outro Castelo de Fadas                |
|     ***Retorno*** | Pericles precisa tomar cuidado com o Monstro dentro  dele         |
|   ***Adaptação*** | Bobby faz tantas coisas erradas, mas sempre pelos motivos certos… |

## Perícias
 
|         ***Nível*** | ***Perícia*** | ***Perícia*** | ***Perícia*** | ***Perícia*** |
|--------------------:|:-------------:|:-------------:|:-------------:|:-------------:|
|    ***Ótimo (+4)*** | Recursos      |               |               |               |
|      ***Bom (+3)*** | Conhecimentos | Comunicação   |               |               |
| ***Razoável (+2)*** | Contatos      | Empatia       | Ofícios       |               |
|  ***Regular (+1)*** | Atletismo     | Percepção     | Investigar    | Vontade       |

## Façanhas [Recarga: 3]
 
+ ___Já li sobre isso!___
+ ___Assessoria Técnica:___ se conseguir passar em um teste de Recursos com dificuldade igual ao teste original, pode colocar 1 Invocação Gratuita em qualquer Aspecto que tenha sido previamente gerado por uma Ação de _Criar Vantagem_ por _Conhecimentos_ (2 em caso de Sucesso com Estilo).
+ __*Bibbity-Bobbity-Boo* (Permissão: *Escopo da Fada Madrinha*):__ pode alterar magicamente coisas com um teste de _Criar Vantagens_ por _Conhecimentos_, com dificuldade dependendo das alterações a serem feitas (quanto mais alterações ou mais extravagantes, maior a dificuldade). Criaturas Vivas podem Defender-se disso como se fosse um Ataque Mental. Em caso de Falha, pode demandar queima de Condições de Escopo ou fazer o alvo ficar meio maluco. A alteração dura uma cena ou até que um evento específico determinado no momento do teste ocorra (_"Quando o relógio badalar as doze horas..."_). Pode também realizar usar sua Magia para Superar obstáculos que possam ser afetados pela mesma.

## Estresse

+ ___Estresse:___
    + ___Físico:___ (1)(2)( )( )
    + ___Mental:___ (1)(2)(3)( )
+ ___Consequências:___
    + ___Suave (2):___ 
    + ___Moderada (4):___ 
    + ___Severa (6):___ 
+ ___Condições:___
    + ___Escopo:___ ( )( )( )( )( )
